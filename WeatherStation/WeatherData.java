package WeatherStation;
import java.util.*;

public class WeatherData implements Subject{
	private ArrayList observers;
	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData() {
		observers = new ArrayList();
		
	}
	
	public void registerObserver(ObserverHFDS o)
	{
		observers.add(o);
	}
	
	public void removeObserver(ObserverHFDS o)
	{
		int i = observers.indexOf(o);
		if(i >= 0)
		{
			observers.remove(i);
		}
	}
	
	public void notifyObservers()
	{
		for(int i =0; i < observers.size(); i++)
		{
			ObserverHFDS observer = (ObserverHFDS)observers.get(i);
			observer.update(temperature, humidity, pressure);
		}
	}
	
	public void measurementsChanged()
	{
		notifyObservers();
	}
	
	public void setMeasurements(float temperature, float humidity, float pressure)
	{
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	
}
