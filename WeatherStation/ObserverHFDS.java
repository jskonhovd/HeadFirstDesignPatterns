package WeatherStation;

public interface ObserverHFDS {
	public void update(float temp, float humidity, float pressure);
}
