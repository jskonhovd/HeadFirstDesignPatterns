package WeatherStation;


public interface Subject {
	public void registerObserver(ObserverHFDS o);
	public void removeObserver(ObserverHFDS o);
	public void notifyObservers();
	
}

