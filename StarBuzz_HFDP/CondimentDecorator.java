package StarBuzz_HFDP;

public abstract class CondimentDecorator extends Beverage {
	public abstract String getDescription();

}
