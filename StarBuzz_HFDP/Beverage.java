package StarBuzz_HFDP;
import java.util.*;
public abstract class Beverage {

	String description = "Jeff's Coffee";
	
	public String getDescription() {
		return description;
	}
	
	public abstract double cost();
	
}
